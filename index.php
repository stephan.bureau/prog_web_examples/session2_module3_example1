<html>

<head>
  <base href="/" />
  <title>Ceci est mon titre</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>
  <?php
  $host = '127.0.0.1';
  $db   = 'testdb1';
  $user = 'admin';
  $pass = 'admin';
  $charset = 'UTF8';
  $connectionString = "mysql:host=$host;dbname=$db;charset=$charset";
  try {
    $pdo = new PDO($connectionString, $user, $pass);
  } catch (PDOException $e) {
    throw new PDOException($e->getMessage(), (int) $e->getCode());
  }
  ?>

  <h2>Demo 1</h2>
  <label>Noms :</label>
  <ul>
    <?php
    $stmt = $pdo->query('SELECT name FROM contact');
    while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
      echo "<li>" . $row->name . "</li>";
    }
    ?>
  </ul>

  <hr>

  <h2>Demo 2</h2>
  <i>Nom et adresse du contact id=2 :</i>
  <p>
    <?php
    $contactId = "2";
    $stmt = $pdo->prepare('SELECT * FROM contact WHERE id = :contactId');
    $stmt->execute(['contactId' => $contactId]);
    $user = $stmt->fetch(PDO::FETCH_OBJ);
    echo $user->name . ' - ' . $user->adress;
    ?>
  </p>

  <hr>

  <h2>Demo 3</h2>
  <?php
  if (isset($_GET['isContactCreated']) && $_GET['isContactCreated'] == true) {
    echo "<p style=\"color:green;background-color: #ddd;padding: 4px;\">New contact created successfully!</p>";
  }
  ?>
  <i>Nom et adresse du nouveau contact :</i>
  <form action="/createContact.php" method="POST">
    <input name="name" type="text" placeholder="Nom du contact" />
    <input name="adress" type="text" placeholder="Adresse du contact" />
    <input type="submit" value="Créer">
  </form>
</body>

</html>