<?php
if (!isset($_POST["name"]) || !isset($_POST["adress"])) {
  header("Location: /index.php");
}

// store POST parameters in variables
$name = $_POST["name"];
$adress = $_POST["adress"];

$host = '127.0.0.1';
$db   = 'testdb1';
$user = 'admin';
$pass = 'admin';
$charset = 'UTF8';
$connectionString = "mysql:host=$host;dbname=$db;charset=$charset";
try {
  $pdo = new PDO($connectionString, $user, $pass);
} catch (PDOException $e) {
  throw new PDOException($e->getMessage(), (int) $e->getCode());
}

// create new contact from POST parameters
$stmt = $pdo->prepare('INSERT INTO contact (name, adress) VALUES(:name, :adress)');
$stmt->bindParam('name', $name, PDO::PARAM_STR);
$stmt->bindParam('adress', $adress, PDO::PARAM_STR);
$success = $stmt->execute();

header("Location: /index.php?isContactCreated=" . $success);
